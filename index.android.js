/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

import App from './App';

const Calculator = () => (
  <App/>
)

AppRegistry.registerComponent('Calculator', () => Calculator);
