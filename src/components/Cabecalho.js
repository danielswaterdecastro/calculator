import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

export default props => (
    <View style={ estilo.topo }>
        <Text style={ estilo.txtTitulo }>Calculadora 1.1</Text>
    </View>
)

const estilo = StyleSheet.create({
    topo:{
        backgroundColor: '#2196f3',
        padding: 10,
        alignItems: 'center',
    },
    txtTitulo:{
        fontSize: 25,
        color: 'white'

    }
})