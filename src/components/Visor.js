import React from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

export default props => (
    <View>
        <TextInput
        style={ estilo.resultado }
        placeholder="Resultado"
        editable={ false }
        value={ props.resultado }
        />
    </View>
)

const estilo = StyleSheet.create({
    resultado:{
        padding: 30,
        fontSize: 20
    }
})