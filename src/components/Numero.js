import React from 'react';
import { TextInput, StyleSheet } from 'react-native'

export default props => (
    <TextInput 
        style={ estilo.numero } 
        value={ props.num }
        onChangeText={ valorDoCampo => props.atualizaValor(props.nome, valorDoCampo) }
    />
)

const estilo = StyleSheet.create({
    numero:{
        width: 140,
        height: 80,
        fontSize: 20,
    }
})