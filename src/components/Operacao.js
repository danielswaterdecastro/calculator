import React, { Component } from 'react';
import { View, Picker, StyleSheet } from 'react-native';

export default class Operacao extends Component {

    render() {
        return(
            <Picker style={ estilo.operacao }
                selectedValue={ this.props.operacao }
                onValueChange={ op => { this.props.atualizaOperacao(op) } }
                >
                <Picker.Item label='Soma' value='soma'/>
                <Picker.Item label='Subtração' value='subtracao'/>
            </Picker>
        )
    }
}

const estilo = StyleSheet.create({
    operacao:{
        marginTop: 25,
        marginBottom: 25,
    }
})